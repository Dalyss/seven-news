let main = function () {

    $('.icon-menu').click(function () {

        $('.menu').animate({
            left: '0px'
        }, 200);

        $('body').animate({
            left: '285px'
        }, 200);
    });


    $('.icon-close').click(function () {

        $('.menu').animate({
            left: '-285px'
        }, 200);

        $('body').animate({
            left: '0px'
        }, 200);
    });
};

$(document).ready(main);

$(document).ready(function () {
    let $searchClosed = $('.menu__search');
    $searchClosed.on('mouseover', function () {
        $(".menu__search").children(".search").css({"opacity": "1", "width": "15rem"});
    });
    $searchClosed.on('mouseout', function () {
        $(".menu__search").children(".search").css({"opacity": "0", "width": "0rem"});
    });


    let swiperCarousel = new Swiper('.swiper-container--carousel', {
        slidesPerView: 3,
        spaceBetween: 0,
        slidesPerGroup: 3,
        loop: true,

        pagination: {
            el: '.swiper-pagination',
        },
        breakpointsInverse: true,
        breakpoints: {
            0: {
                slidesPerView: 2,
                spaceBetween: 0,
                slidesPerGroup: 2,
            },
            480: {
                slidesPerView: 2,
                spaceBetween: 0,
                slidesPerGroup: 2,
            },
            1060: {
                slidesPerView: 3,
                spaceBetween: 0,
                slidesPerGroup: 3,
            }
        },
    });

    let swiperSlider = new Swiper('.swiper-container--slider', {
        slidesPerView: 1,
        spaceBetween: 0,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });
});

let mainSwiper = new Swiper('.swiper-container', {
    slidesPerView: 5,
    spaceBetween: 0,
    slidesPerGroup: 5,
    direction: 'horizontal',
    loopFillGroupWithBlank: true,
    loop: true,
    breakpointsInverse: true,
    breakpoints: {
        0: {
            slidesPerView: 2,
            spaceBetween: 0,
            slidesPerGroup: 2,
        },
        480: {
            slidesPerView: 3,
            spaceBetween: 0,
            slidesPerGroup: 3,
        },
        620: {
            slidesPerView: 4,
            spaceBetween: 0,
            slidesPerGroup: 4,
        },
        905: {
            slidesPerView: 5,
            spaceBetween: 0,
            slidesPerGroup: 5,
        }
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    }
});




